json.extract! employee, :id, :name, :last_name, :dni, :phone_number, :email, :bank, :account_type, :account_number, :phone_number_2, :phone_emergency, :department_id, :created_at, :updated_at
json.url employee_url(employee, format: :json)
